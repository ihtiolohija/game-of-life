//--Game of Life--
//--tested in Chrome and Mozilla (in Mozilla works slightly faster :)
//--supports square and non-square figures
//--supports square and non-square boards
//--uses RequestAnimationFrame for animation
'use strict'

//get requestAnimationFrame
window.requestAnimationFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
})();

//take String for Figure and transform it to 2d Array
function buildFigure(type) {
    //create transformation array
    var array = [];
    //check if the string is empty
    if (!type) {
        return 0;
    }
    array = type.split(' '); //take string array separated by spaces
    for (var i = 0; i < array.length; i++) {
        array[i] = array[i].split('');//consider separate array for each row
        for (var j = 0; j < array[i].length; j++)
            array[i][j] = parseInt(array[i][j], 10);//parse the string to number
    }
    return array;//return 2d number array representation for figure
}
//constructor for Figure (takes the String and keeps in array its number representation)
function Figure(type) {
    this.array = [];
    this.array = buildFigure(type);
}

//consider the game object
var life = {

    //set canvas and context params for the board
    canvas: document.getElementById("canvas"),
    ctx: document.getElementById("canvas").getContext('2d'),

    //the size of the figure cells for drawing
    x_size: 5,
    y_size: 7,

    generation: 0, //number of current generation

    color: '',//default color for the generation

    figures: [],//empty array for figures

    next: [],//empty array for the next generation

    live_cells: [], //empty array to store live cells coordinates

    //generate 2d array filled with zeros
    buildArray: function () {
        var array = [];//make empty array
        for (var i = 0; i < this.width; i++) {
            var innerArr = [];//make inner array
            for (var j = 0; j < this.height; j++) {
                innerArr.push(0);//fill inner array with zeros
            }
            array.push(innerArr);//push inner array with each row
        }
        return array;
    },
    //initialize the game with given width and height
    init: function (width, height) {
        //set given parameters for the board
        this.width = width;
        this.height = height;
        //set canvas size slightly bigger than actual board size
        this.canvas.width = width + 2 * this.x_size;
        this.canvas.height = height + 2 * this.y_size;
        //set up the initial array for the empty board
        this.memory = this.buildArray();

    },
    //create figures and add them to the board
    addFigures: function () {
        //create several figures from the Strings
        var block = new Figure("0111 0111 0111");//create block figure
        var glider = new Figure("010 001 111");//create glider
        //create custom figure
        var pulsar = new Figure("0011100011100 0000000000000 1000010100001 1000010100001" +
            " 1000010100001 0011100011100 0000000000000 0011100011100 " +
            "1000010100001 1000010100001 1000010100001 0000000000000 0011100011100");

        var custom_figure = new Figure("111000 111000 111000 000111 000111 000111");
        var pulsar_2 = new Figure("0011100011100 0000000000000 1000010100001 1000010100001" +
            " 1000010100001 0011100011100 0000000000000 0011100011100 " +
            "0000000000000 1000010100001 1000010100001 1000010100001 0000000000000 0011100011100");
        //assume the created figures to the figures array for the board
        //consider the dimensions: populate the 1/4 of the board to avoid the overflow
        for (var i = 0; i < this.memory.length / 4; i += 5) {
            this.figures[i] = pulsar;
            this.figures[i + 1] = custom_figure;
            this.figures [i + 2] = pulsar_2;
            this.figures[i + 3] = glider;
            this.figures[i + 4] = block;
        }
        //place figures randomly on the board
        this.placeFiguresRandomly();
    },

    //generate random color
    getRandomColor: function () {
        //return the string with 3 randomly generated color values
        return 'rgb(' + Math.round(255 * Math.random()) + ',' + +Math.round(255 * Math.random()) + ',' + Math.round(255 * Math.random()) + ')';
    },

    //make counter for monitoring the number of function calls
    c: (function () {
        var counter = 0;
        return function () {
            counter += 1;//each call, increment the counter
            return counter;
        }
    })(),

    //get random coordinates for the specified figure
    getRandomXY: function (w, h) {
        var n = this.c(); //get the number of function calls
        console.log("func has been called times: " + n);
        console.log("memory length is: " + this.memory.length);
        //if the figure doesn't fit in the board, return 0
        //if the function is called too much, return 0
        if (n >= this.memory.length || w > this.memory[0].length || h > this.memory.length) {
            return 0;
        }

        //generate random coordinates
        var x = Math.floor(Math.random() * this.width / 4);
        var y = Math.floor(Math.random() * this.height / 4);

        //if the generated coordinates are not fit on the edges of the board, generate again
        if (x + w > this.memory.length || y + h > this.memory[0].length || x - w < 0 || y - h < 0) {
            this.getRandomXY(w, h);

        }

        //if the generated position is already busy, generate again
        if (this.memory[x][y]) {
            this.getRandomXY(w, h);
        }

        //return generated coordinates
        return {
            i: x,
            j: y
        };

    },

    //place figures randomly on the board
    placeFiguresRandomly: function () {
        //check if the figures array not empty
        if (!this.figures) {
            return 0;
        }

        //copy current object and work with the copy within the function
        var that = this;
        //for each figure from the figures array
        that.figures.forEach(function (figure) {
            //check if the figure object is empty
            if (typeof figure.array.length == 'undefined') {
                return 0;
            }

            var figureWidth = figure.array.length; //get width of the figure
            var figureHeight = figure.array[0].length; // get height of the figure
            var x, y = 0;
            //get the generated random coordinates
            var dimensions = that.getRandomXY(figureWidth, figureHeight);
            //if the returned value is 0, stop executing
            if (!dimensions) {
                return 0;
            }
            //get generated X and Y position
            x = dimensions.i;
            y = dimensions.j;
            //place the figure on the board in the generated position
            for (var i = 0; i < figureWidth; i++) {
                for (var j = 0; j < figureHeight; j++) {
                    that.memory[x + i][y + j] = figure.array[i][j];
                    //check if the board is full
                    if (that.live_cells.length == that.memory.length * that.memory[0].length) {
                        return 0;
                    }
                    //store live cells coordinates
                    if (figure.array[i][j] == 1)
                        that.live_cells.push({
                                live_x: x + i,
                                live_y: y + j
                            }
                        );
                }
            }
        });
        //write updated matrix with figures to the current matrix
        this.memory = that.memory;
        this.live_cells = that.live_cells;//write generated live cells array to the current object
        this.drawLiveCells();//draw live cells
    },

    //clean the specified array
    emptyArray: function (array) {
        while (array.length > 0)
            array.pop();
    },
    //draw live cells on the board
    drawLiveCells: function () {
        //clear canvas to draw next generation
        life.ctx.clearRect(0, 0, life.canvas.width, life.canvas.height);
        //display background canvas
        this.display();
        //display counter of the generations
        this.displayCounter();
        //traverse live cells array and color each live cell
        //if the live cells is empty
        if (!this.live_cells) {
            return 0;
        }
        this.live_cells.forEach(function (live_cell) {
            //set params for drawing the cell
            life.ctx.beginPath();
            //draw rect with given cell sizes in the specified position
            life.ctx.rect(live_cell.live_x * life.x_size, live_cell.live_y * life.y_size, life.x_size, life.y_size);
            //get current generation color
            life.ctx.fillStyle = life.color;
            life.ctx.closePath();
            life.ctx.fill();//fill the cell
        });
        this.emptyArray(this.live_cells); //clear the live cells array

    },

    //calculate next generation of the game
    generateNext: function () {
        //fill next generation array with zeros
        this.next = this.buildArray();
        //for each cell, count the neighbours
        for (var x = 0; x < this.next.length; x++) {
            for (var y = 0; y < this.next[0].length; y++) {
                var cell = this.memory[x][y];//get the current cell
                var live_cells = this.countNeighbors(x, y);//get neighbours number for the cell
                //check if the cell is dead or alive
                //write the live cells to live cells array
                if (cell == 1) {
                    if (live_cells == 2 || live_cells == 3) {
                        this.next[x][y] = 1;
                        this.live_cells.push({
                                live_x: x,
                                live_y: y
                            }
                        );
                    }
                }
                if (cell == 0) {
                    if (live_cells == 3) {
                        this.next[x][y] = 1;
                        this.live_cells.push({
                                live_x: x,
                                live_y: y
                            }

                        );
                    }
                }
            }
        }

        this.color = this.getRandomColor();//for each generation, generate different color
        this.generation = this.generation + 1;//increase the number of generation
        this.memory = this.next;//update current matrix with the next generation matrix

        return this.memory;
    },

    //get the number of the neighbours for the specified cell
    countNeighbors: function (x, y) {
        var n = 0;
        //get number of live cells around the position
        if (x > 0 && y > 0 && x < this.memory.length - 1 && y < this.memory[0].length - 1) {
            n = this.getMooreNeighborhood(x, y);
        }
        return n;

    },

//get Moore Neighborhood (number of live cells around the specified cell)
    getMooreNeighborhood: function (x, y) {
        var g = 0;
        var k = 0;
        //traverse the cells around
        for (var i = y - 1; i < y + 2; i = i + 2) {
            for (var j = x - 1; j < x + 2; j++) {
                if (this.memory[j][i]) {
                    g += this.memory[j][i];
                }
            }
        }
        //traverse the cells around in the same row
        for (var i = x - 1; i < x + 2; i = i + 2) {
            if (this.memory[i][y]) {
                k += this.memory[i][y];
            }
        }

        return g + k;

    },

    //display the background of the game
    display: function () {
        //color the canvas with black
        this.ctx.fillStyle = 'black';
        this.ctx.rect(0, 0, this.canvas.width, this.canvas.height);
        //this.ctx.arc(this.canvas.width/2,this.canvas.height/2 , this.canvas.height/2, 0, 2*Math.PI, true);
        this.ctx.fill();
    },

    //display generations counter
    displayCounter: function () {
        //get canvas and context params for displaying counter
        var counter = document.getElementById("counter");
        var context = counter.getContext('2d');
        //clear the space for displaying new value
        context.clearRect(0, 0, counter.width, counter.height);
        context.font = '18pt Calibri';
        context.fillStyle = 'black';
        //display the current generation number
        context.fillText("Generation " + this.generation, 50, 100);
    }

}

//function for smart animating
function animate() {
    requestAnimationFrame(animate);
    life.generateNext();//each time, get next generation
    life.drawLiveCells();//each time, draw live cells on the board


}

life.init(700, 500);//initialize the board 700x500
life.addFigures();//create and add figures to the board
animate();//call the animation function


//-----UNIT TESTS------
//note: comment the calls from above, uncomment functions from the very bottom of the page
//make an object to collect the tests log
var test_result = {
    total: 0,//number of total tests
    bad: 0 //number of failed tests
}
//compare two given matrix
function compare_generations(initial, provided) {
    //compare cell by cell
    for (var i = 0; i < initial.length; i++)
        for (var j = 0; j < initial.length; j++)
            if (initial[i][j] == provided[i][j])
                return 1;
    return 0;
}

//test the algorithm of the game on the blinker example
function test_algorithm() {
    //check correctness of the algorithm on the blinker figure
    life.init(10, 10);//initialize the board
    var blinker = new Figure("001 001 001");//create blinker
    life.figures = [blinker];//add blinker to the figures array for the board
    var initial = life.memory;
    life.generateNext();//generate second generation matrix
    var period_2 = life.generateNext();//get the third generation matrix
    //get the report (the initial and the third generations must be equal)
    var v = compare_generations(initial, period_2);
    runTest(v, 1);//compare the given and expected results
}
//test the figure - related stuff
function test_figures() {
    life.init(10, 10);//initialize again to clear old data
    var empty_figure = new Figure();//make empty figure
    life.figures = [empty_figure];
    var v = life.placeFiguresRandomly(); //get the return value
    //generate report (when the figure is not presented, must be undefined)
    runTest(v, undefined);//check if return value is 0
    //clear figures array
    life.emptyArray(life.figures);
    var v1 = life.placeFiguresRandomly();
    //(when there are no live cells and the figures array is empty, must be undefined)
    runTest(v1, undefined);
}

//test for board overflow
function test_board() {
    //check what happens when the figure is bigger than the board (overflow)
    //make 13x13 figure
    life.init(10, 10);
    var big_figure = new Figure("0011100011100 0000000000000 1000010100001 1000010100001" +
        " 1000010100001 0011100011100 0000000000000 0011100011100 " +
        "0000000000000 1000010100001 1000010100001 1000010100001 0011100011100");
    life.figures = [big_figure];
    var v = life.placeFiguresRandomly();//get the return value
    //generate report(when the figure doesn't fit the board, must be undefined)
    runTest(v, undefined);
}

//generate test report
function runTest(provided, expected) {
    test_result.total++;
    if (provided == expected)
        console.log("test passed");
    else
        test_result.bad++;

    console.log("Of " + test_result.total + " tests, " + test_result.bad + " failed, " + (test_result.total - test_result.bad) + "passed");

}

//test_algorithm();
//test_board();
//test_figures();